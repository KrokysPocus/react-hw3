import { NavLink } from 'react-router-dom';
import styles from './NavBar.module.scss';
import { listNavigation } from '../../helpers/listNavigation';
const NavBar = () => {
  
  return (
    <nav className={styles.nav}>
      <ul>
        {listNavigation.map((item) => (
          <li key={item}>
            <NavLink
              to={item}
              className={({ isActive }) =>
                isActive ? `${styles.activeLink}` : ''
              }
            >
              { item}
            </NavLink>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default NavBar;

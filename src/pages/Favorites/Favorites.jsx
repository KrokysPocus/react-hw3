import { useLocation, Link } from 'react-router-dom';
import { AiOutlineRight, AiOutlineClose } from 'react-icons/ai';
import styles from './Favorites.module.scss'

const Favorites = ({ products, favoritesId, addToCard, toggleFavorite }) => {
  const favoriteProducts = products.filter((prod) =>
    favoritesId.find((item) => prod.id === item)
  );
  const location = useLocation();
  const pathParts = location.pathname.split('/').filter((part) => part !== '');
  const breadCrumbs = pathParts.map((item, index) => {
    if (index === pathParts.length - 1) {
      return (
        <span className="currentPage" key={index}>
          {item}
        </span>
      );
    }
    return (
      <Link to=".." relative="path" key={index}>
        {' '}
        {item} <AiOutlineRight />
      </Link>
    );
  });
  const renderFavoriteProducts = favoriteProducts.map((product) => {
    return (
      <div className={styles.favProduct} key={product.id}>
        <div className={styles.wrapperProductData}>
          <span>
            <AiOutlineClose
              className={styles.removeFromFav}
              onClick={() => toggleFavorite(product.id)}
            />
          </span>
          <img src={product.imageUrl} alt={product.name} />
          <div className={styles.productData}>
            <h4>{product.name}</h4>
            <div className={styles.productInfo}>
              Color:
              <span className={styles.productInfoValue}>{product.color}</span>
            </div>
            <div className={styles.productInfo}>
              Quantity:
              <span className={styles.productInfoValue}>{product.color}</span>
            </div>
          </div>
        </div>
        <div className={styles.productDataCart}>
          <span className={styles.productPrice}>{product.price}$</span>
          <button className="mainBtn" onClick={() => addToCard(product)}>
            Add to cart
          </button>
        </div>
      </div>
    );
  });
  return (
    <div className="wrapper">
      <div className="breadCrumbs">
        <Link to="/">
          Home <AiOutlineRight />
        </Link>
        {breadCrumbs}
      </div>
      <h2 className={styles.favTitle}>Favorites</h2>
      <div className={styles.wrapperFavoriteProducts}>
        {renderFavoriteProducts}
      </div>
    </div>
  );
};

export default Favorites;
